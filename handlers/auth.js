const db = require("../models");
const passport = require('passport');


module.exports.login = function(req,res) {
    const user = new db.User({
      username: req.body.username,
      password: req.body.password
    });
  
    req.login(user, function(err) {
      if(err){
        console.log(err);
      }else{
        passport.authenticate("local")(req,res,function() {
          res.redirect("/home");
        })
      }
    })
}

module.exports.register = function(req,res) {

    db.User.register({
      username:req.body.username,
      firstName: req.body.fname,
      lastName: req.body.lname,
      phoneNumber: req.body.pn
    
    }, req.body.password,function(err,user) {
      if(err){
        console.log(err);
        res.redirect("/register");
      }else{
        passport.authenticate("local")(req,res,function() {
          res.redirect("/home")
        });
      }
    });
  }