const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const findOrCreate = require('mongoose-findorcreate');

const userSchema = new mongoose.Schema( {
    username:  {
      type: String,
      unique: true,
      createIndexes: true,
      required: true
    },
    firstName: {
      type: String,
      createIndexes: true,
    },
    lastName: {
      type: String,
      createIndexes: true
    },
    phoneNumber: {
      type: String,
      unique: true,
      createIndexes: true
    },
    password: {
      type: String,
      createIndexes: true,
    },
    googleId: {
      type: String,
      createIndexes: true
    },
    resetPasswordToken: String,
    resetPasswordExpires: Date,
});
userSchema.plugin(passportLocalMongoose);
userSchema.plugin(findOrCreate);

const User = mongoose.model("User", userSchema);
module.exports = User;